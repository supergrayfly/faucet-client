const abi = require('./../assets/abi-erc20.json')
const contracts = require("@ethersproject/contracts");
const providers = require("@ethersproject/providers");
const provider = new providers.Web3Provider(window.ethereum, 'any');
const signer = provider.getSigner();

const addr_matic = "0xDFD148664Cf16d1c2C88e0f1b6420b1e2DD6cc06";
const addr_btc = "0x0E7097623781d83c4bDF4d8C82996C3f12A268eb";
const addr_eth = "0xF285642B5Ea88dF2850977A2F4cd058a214c3086";
const addr_usdt = "0x6B0734c6a30a1b580A630C9f1A722Da763279E9a";
const addr_ftm = "0x0C9bC941c0F1AE9c00beCe968e629362ad37bcf5";

const matic = new contracts.Contract(addr_matic, abi, signer);
const ftm = new contracts.Contract(addr_ftm, abi, signer);
const btc = new contracts.Contract(addr_btc, abi, signer);
const eth = new contracts.Contract(addr_eth, abi, signer);
const usdt = new contracts.Contract(addr_usdt, abi, signer);

//Connect Wallet to App
document.addEventListener('DOMContentLoaded', async () => {
	console.log('Welcome to Spectrr Finance Faucet.')
});

document.getElementById('connect').addEventListener('click', async () => {
  let accounts = await ethereum.request({ method: 'eth_accounts' });

  if (accounts.length == 0) {
    ethereum.request({ method: 'eth_requestAccounts' }).catch((error) => {
      if (error.code === 4001) {
        // EIP-1193 userRejectedRequest error
        console.log(error.message);
      } else {
        console.log('Connecting MetaMask Failed.');
      }
    });
  } else {
    console.log('Wallet already connected!');
  }
});

document.getElementById('dripftm').addEventListener('click', async () => {
  await ftm.drip();
});

document.getElementById('dripmatic').addEventListener('click', async () => {
  await matic.drip();
});

document.getElementById('dripbtc').addEventListener('click', async () => {
  await btc.drip();
});

document.getElementById('dripeth').addEventListener('click', async () => {
  await eth.drip();
});

document.getElementById('dripusdt').addEventListener('click', async () => {
  await usdt.drip();
});
