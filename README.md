# Spectrr Finance Testnet Faucet Web Client

Faucet for Spectrr Finance Testnet. It can drip the following tokens:
- Bitcoin (BTC)
- Fantom (FTM)
- Matic (MATIC)
- Ethereum (ETH)
- Usdt (USDT)

# Installation

To get the client working on your machine, make sure
you have nodejs and npm (or any other package manager)
installed. 
Also, you need to have a web3 wallet for your browser,
such as Metamask and Exodus. 
You can then do:

```
> git clone https://gitlab.com/supergrayfly/faucet-client
> cd faucet-client
> npm install
> npm run server
```
By default, the client will be running at https://localhost:4200

# Development

If you want to modify the faucet source code,
you need to install the development dependencies:

```
> git clone https://gitlab.com/supergrayfly/faucet-client
> cd faucet-client
> npm install --save-dev
// make changes
> npm run build 
> npm run server
```

# Disclaimer

- TOKENS HAVE ABSOLUTELY NO REAL VALUE,
THEY ARE ONLY USED FOR TESTING PURPOSE.

# Contributing

Feel free to contribute in any way to this project.
Pull requests, comments, and emails are all appreciated.

# Contact

email: supergrayfly@proton.me

# License

BSD-3-Clause
